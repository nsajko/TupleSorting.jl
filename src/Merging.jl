# Copyright © 2023 Neven Sajko. All rights reserved.

module Merging

import ..TupleUtils

const Ord = Base.Order

const NT           = TupleUtils.NT
const OneAndUp     = TupleUtils.OneAndUp
const tup_tail     = TupleUtils.tup_tail
const cat          = TupleUtils.cat
const tuple_slice  = TupleUtils.tuple_slice
const rt           = TupleUtils.rt
const deinterleave = TupleUtils.deinterleave
const   interleave = TupleUtils.interleave

struct SmallSize end
struct SmallDepth end
struct Oblivious end
struct Stable end

function even_odd_clean(mm::Mm, t::NT{n}) where {Mm, n}
  iseven(n) || error("expected even, got $n")
  f = let mm = mm, t = t
    function(i::Int)
      j = i - firstindex(t)
      k = 2 * j
      mm(t[begin + k], t[begin + k + 1])
    end
  end
  TupleUtils.flatten(ntuple(f, Val(n >>> true)))::rt(t)
end

function even_odd_clean_abs(
  mm::Mm, t::NT{n}, ::First, ::Last,
) where {Mm, n, first, last, First<:Val{first}, Last<:Val{last}}
  s = tuple_slice(t, First(), Last())
  f = let x = even_odd_clean(mm, s), t = t
    i -> (first ≤ i ≤ last) ? x[i - first + 1] : t[i]
  end
  ntuple(f, Val(n))::rt(t)
end

# `f` is the count of values in the front that are just forwarded,
# without a comparator being applied to them. `b` is the same for the
# back, instead of the front.
function even_odd_clean_rel(mm::Mm, t::NT{n}, ::Val{f}, ::Val{b}) where {Mm, n, f, b}
  k = Val(firstindex(t) + f)
  l = Val( lastindex(t) - b)
  even_odd_clean_abs(mm, t, k, l)::rt(t)
end

merge_stable(::Tuple{}, ::Tuple{}, ::Ord.Ordering) = ()
merge_stable(::Tuple{}, t::OneAndUp, ::Ord.Ordering) = t
merge_stable(t::OneAndUp, ::Tuple{}, ::Ord.Ordering) = t

merge_oblivious_ss(::Tuple{}, ::Tuple{}, ::Any) = ()
merge_oblivious_ss(::Tuple{}, t::OneAndUp, ::Any) = t
merge_oblivious_ss(t::OneAndUp, ::Tuple{}, ::Any) = t

merge_oblivious_ss(l::Tuple{L}, r::Tuple{R}, mm::Mm) where {L, R, Mm} =
  mm(only(l), only(r))::rt(l, r)

merge_oblivious_sd(::Tuple{}, ::Tuple{}, ::Any) = ()
merge_oblivious_sd(::Tuple{}, t::OneAndUp, ::Any) = t
merge_oblivious_sd(t::OneAndUp, ::Tuple{}, ::Any) = t

merge_oblivious_sd(l::Tuple{L}, r::Tuple{R}, mm::Mm) where {L, R, Mm} =
  mm(only(l), only(r))::rt(l, r)

comparator_count_check(::V, ::V) where {V<:Val} = nothing

comparator_count_check(::Val{e}, ::Val{g}) where {e, g} =
  error("wrong comparator count, expected $e, got $g")

function merge_oblivious_ss(
  l::L, r::R, mm::Mm,
) where {m, n, L<:OneAndUp{m}, R<:OneAndUp{n}, Mm}
  # Knuth, TAOCP, Volume 3, section 5.3.4, "generalized Batcher's merge
  # exchange sort"
  (s, t) = deinterleave(l)
  (u, v) = deinterleave(r)
  x = merge_oblivious_ss(s, u, mm)
  y = merge_oblivious_ss(t, v, mm)
  total = 2 + m + n
  start = 1
  fin = total - (start + 2*((total - 1) >>> true))
  let exp = (total - 1) >>> true, got = (total - (start + fin)) >>> true
    comparator_count_check(Val(exp), Val(got))
  end
  T = rt(l, r)
  i = interleave(x, y)::T
  even_odd_clean_rel(mm, i, Val(start), Val(fin))::T
end

conditionally_split_single_from_end(::Val{false}, x::T) where {T<:Tuple} = (x, ())
conditionally_split_single_from_end(::Val{true},  x::T) where {T<:OneAndUp} =
  (tuple_slice(x, Val(firstindex(x)), Val(lastindex(x) - 1)), (last(x),))

function merge_oblivious_sd(
  l::L, r::R, mm::Mm,
) where {m, n, L<:OneAndUp{m}, R<:OneAndUp{n}, Mm}
  # Knuth, TAOCP, Volume 3, section 5.3.4, exercise 57, "even-odd merge"
  (s, t) = TupleUtils.deinterleave_backwards(l)
  (u, v) = deinterleave(r)
  x = merge_oblivious_sd(s, u, mm)
  y = merge_oblivious_sd(t, v, mm)
  total = 2 + m + n
  (start, fin) = map(iseven, (m + 1, n + 1))
  cld = n -> n - (n >>> true)
  let exp = cld(m + 1) + cld(n + 1) - 1, got = (total - (start + fin)) >>> true
    comparator_count_check(Val(exp), Val(got))
  end
  T = rt(l, r)
  (y1, y2) = conditionally_split_single_from_end(Val(fin), y)
  i = interleave(x, y1)
  res = even_odd_clean_rel(mm, i, Val(start), Val(0))
  TupleUtils.concat(res, y2)::T
end

merge_oblivious_impl(
  l::L, r::R, mm::Mm, ::SmallDepth,
) where {L<:Tuple, R<:Tuple, Mm} =
  merge_oblivious_sd(l, r, mm)

merge_oblivious_impl(
  l::L, r::R, mm::Mm, ::SmallSize,
) where {L<:Tuple, R<:Tuple, Mm} =
  merge_oblivious_ss(l, r, mm)

new_minmax(o::O) where {O<:Ord.Ordering} = let o = o
  function(l, r)
    Ord.lt(o, r, l) ? (r, l) : (l, r)
  end
end

merge_oblivious(
  l::L, r::R, o::O, sd::SD,
) where {L<:Tuple, R<:Tuple, O<:Ord.Ordering, SD} =
  merge_oblivious_impl(l, r, new_minmax(o), sd)

function merge_stable(
  l::L, r::R, o::O,
) where {n, m, L<:OneAndUp{n}, R<:OneAndUp{m}, O<:Ord.Ordering}
  a = first(l)
  b = first(r)
  T = NT{1 + n + m}
  if !Ord.lt(o, b, a)
    let x = merge_stable(tup_tail(l)::NT{n}, r, o)::T
      cat(a, x)
    end
  else
    let x = merge_stable(l, tup_tail(r)::NT{m}, o)::T
      cat(b, x)
    end
  end::rt(l, r)
end

merge(l::L, r::R, o::O, ::Stable) where {L<:Tuple, R<:Tuple, O<:Ord.Ordering} =
  merge_stable(l, r, o)

merge(
  l::L, r::R, o::O, d::Tuple{Oblivious,Any},
) where {L<:Tuple, R<:Tuple, O<:Ord.Ordering} =
  merge_oblivious(l, r, o, last(d))

end
