# Copyright © 2023 Neven Sajko. All rights reserved.

module BinaryTrees

struct Node{T,Children<:Tuple}
  v::T
  children::Children

  Node{T,C}(v::T, c::C) where {T, C<:Tuple} = new{T,C}(v, c)
end

subtrees(r::N) where {N<:Node} = r.children

const Children = Union{
  Tuple{Nothing,Nothing},
  Tuple{Nothing,Node},
  Tuple{Node,Nothing},
  Tuple{Node,Node},
}

new_binary_tree(v::T, c::C = (nothing, nothing)) where {T, C<:Children} = Node{T,C}(v, c)

end
