# Copyright © 2023 Neven Sajko. All rights reserved.

module TupleUtils

const NT = NTuple{n,Any} where {n}
const OneAndUp = Tuple{Any,Vararg{Any,n}} where {n}
const TwoAndUp = Tuple{Any,Any,Vararg{Any,n}} where {n}

rt(::T) where {n, T<:NT{n}} = NTuple{n,eltype(T)}
rt(::L, ::R) where {m, n, L<:NT{m}, R<:NT{n}} = NTuple{m + n, Union{eltype(L), eltype(R)}}

# Optimized version of `(e, t...)`.
function cat(e, t::NT{n}) where {n}
  f = let e = e, t = t
    (i::Int) -> isone(i) ? e : t[i - 1]
  end
  ntuple(f, Val(n + 1))::rt((e,), t)
end

# Optimized version of `(l..., r...)`.
function concat(l::L, r::R) where {m, n, L<:NT{m}, R<:NT{n}}
  f = let l = l, r = r
    (i::Int) -> (i ≤ m) ? l[i] : r[i - m]
  end
  ntuple(f, Val(m + n))::rt(l, r)
end

# Optimized version of `Base.tail`.
function tup_tail(x::OneAndUp{n}) where {n}
  f = let x = x
    (i::Int) -> x[i + 1]
  end
  ntuple(f, Val(n))::NT{n}
end

function flatten(t::NTuple{n,NT{2}}) where {n}
  f = let t = t
    function(i::Int)
      j = i - firstindex(t)
      k = j >>> true
      l = j % Bool
      t[begin + k][begin + l]
    end
  end
  ntuple(f, Val(2*n))
end

function tuple_slice(
  t::OneAndUp{nm1}, ::Val{first}, ::Val{last},
) where {nm1, first, last}
  m = last - first + 1
  (m < 0) && error("expected nonnegative, got $m")
  let n = nm1 + 1
    (m ≤ n) || error("expected ordered, got ($m, $n)")
  end
  f = let t = t
    (i::Int) -> t[first + i - 1]
  end
  ntuple(f, Val(m))
end

function interleave(l::L, r::R) where {m, L<:NT{m}, R<:NT{m}}
  f = let l = l, r = r
    function(i::Int)
      j = i - 1
      k = j >>> true
      iseven(j) ? l[begin + k] : r[begin + k]
    end
  end
  ntuple(f, Val(2*m))::rt(l, r)
end

function interleave_impl(l::L, r::R, ::Val{false}) where {m, n, L<:NT{m}, R<:NT{n}}
  (m ≤ n) && error("internal error")
  x = lastindex(l) - (m - n)
  l_core = tuple_slice(l, Val(firstindex(l)), Val(x))::NT{n}
  l_rest = tuple_slice(l, Val(x + 1), Val(lastindex(l)))::NT{m - n}
  concat(interleave(l_core, r), l_rest)
end

function interleave_impl(l::L, r::R, ::Val{true}) where {m, n, L<:NT{m}, R<:NT{n}}
  (n ≤ m) && error("internal error")
  x = lastindex(r) - (n - m)
  r_core = tuple_slice(r, Val(firstindex(r)), Val(x))::NT{m}
  r_rest = tuple_slice(r, Val(x + 1), Val(lastindex(r)))::NT{n - m}
  concat(interleave(l, r_core), r_rest)
end

interleave(l::L, r::R) where {m, n, L<:NT{m}, R<:NT{n}} = interleave_impl(l, r, Val(m < n))

function deinterleave_half(t::T) where {n, T<:OneAndUp{n}}
  m = n + 1
  t::NT{m}
  h = m >>> true
  g = m - h

  f = let t = t
    (i::Int) -> t[begin + 2*(i - 1)]
  end

  ntuple(f, Val(g))
end

deinterleave(          t::Tuple{T}) where {T} = (t, ())
deinterleave_backwards(t::Tuple{T}) where {T} = ((), t)

deinterleave(t::T) where {T<:TwoAndUp} = (
  deinterleave_half(t),
  deinterleave_half(tuple_slice(t, Val(2), Val(lastindex(t)))),
)

deinterleave_backwards(t::T, ::Val{false}) where {T<:TwoAndUp} = deinterleave(t)

deinterleave_backwards(t::T, ::Val{true}) where {T<:TwoAndUp} = (
  deinterleave_half(tuple_slice(t, Val(2), Val(lastindex(t)))),
  deinterleave_half(t),
)

deinterleave_backwards(t::T) where {T<:TwoAndUp} =
  deinterleave_backwards(t, Val(length(t) % Bool))

function split_in_half(t::T) where {n, T<:TwoAndUp{n}}
  m = n + 2
  t::NT{m}
  h = m >>> true
  g = m - h

  fl = let t = t
    (i::Int) -> t[i]
  end

  fr = let t = t, g = g
    (i::Int) -> t[i + g]
  end

  l = ntuple(fl, Val(g))::NT{g}
  r = ntuple(fr, Val(h))::NT{h}

  (l, r)
end

end
