# Copyright © 2023 Neven Sajko. All rights reserved.

module TupleSorting

include("TupleUtils.jl")
include("Merging.jl")
include("BinaryTrees.jl")
include("Sorting.jl")

"""
Indicate that a comparator network should optimize for small depth.
"""
const SmallDepth = Merging.SmallDepth

"""
Indicate that a comparator network should be optimized for small size.
"""
const SmallSize = Merging.SmallSize

"""
Indicate that a merge or sort algorithm need not be stable, but must be
oblivious and implemented based on a comparator network. Use
[`SmallSize`](@ref) or [`SmallDepth`](@ref) to choose the performance
characteristics of the comparator network.
"""
const Oblivious = Merging.Oblivious

"""
Indicate that a stable merge or sort algorithm should be chosen. I.e. the
operation must preserve the order among elements that compare equal.
"""
const Stable = Merging.Stable

"""
Use `StaticLengthSortStable()` to select a stable sorting algorithm adapted for
collections of statically-known length.
"""
const StaticLengthSortStable = Sorting.StaticLengthSortStable

"""
Use `StaticLengthSortOblivious{n,Opt}()` to select a sorting algorithm adapted for
collections of statically-known length. The algorithm is an oblivious merge sort
that optionally uses the sorting networks from the `OptimalSortingNetworks` package
for speeding up the base cases. The sort is not stable.

The value `n`, which must be a nonnegative integer, indicates that the hardcoded
sorting networks from `OptimalSortingNetworks` should be used for sorting
collections containing less than `n` elements, when supported by
`OptimalSortingNetworks`. Choose a large `n`, `100`, for example, to use sorting
networks for all collections supported by `OptimalSortingNetworks`.

The type `Opt` may be [`SmallDepth`](@ref) or [`SmallSize`](@ref).
"""
const StaticLengthSortOblivious = Sorting.StaticLengthSortOblivious

"""
`sorted(col, alg::Base.Sort.Algorithm, ord::Base.Order.Ordering)` returns a copy of
collection `col` sorted according to algorithm `alg` and order `ord`. `alg` and
`ord` are optional, `alg` defaults to `StaticLengthSortStable()` (stable merge
sort), while `ord` defaults to `Base.Order.Forward` (the default order based on
`isless`).
"""
const sorted = Sorting.sorted

export sorted, StaticLengthSortOblivious, StaticLengthSortStable

end
