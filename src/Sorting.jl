# Copyright © 2023 Neven Sajko. All rights reserved.

module Sorting

import OptimalSortingNetworks, ..TupleUtils, ..BinaryTrees, ..Merging

const Ord = Base.Order
const OSN = OptimalSortingNetworks

const NT       = TupleUtils.NT
const TwoAndUp = TupleUtils.TwoAndUp
const rt       = TupleUtils.rt

const SmallDepth = Merging.SmallDepth
const SmallSize  = Merging.SmallSize
const Oblivious  = Merging.Oblivious
const Stable     = Merging.Stable

const new_binary_tree = BinaryTrees.new_binary_tree

function nt_below(v::V) where {V<:Val}
  f = i -> NT{i - 1}
  Union{ntuple(f, v)...}::Type{<:Tuple}
end

supported_osn_below(v::V) where {V<:Val} =
  OSN.supported_types(OSN.sorted, nt_below(v))::Type{<:Tuple}

# For the package to have acceptable performance we need this
# constant-folded. Here's a hack to ensure this for Julia v1.6 LTS.
@static if VERSION < v"1.9"
  length_is_supported_by_osn(::Val{m}, ::Val{n}) where {m, n} =
    (m ≤ min(n - 1, 11))::Bool
else
  length_is_supported_by_osn(::Val{m}, ::Val{n}) where {m, n} =
    (NT{m} <: supported_osn_below(Val(n)))::Bool
end

struct StaticLengthSortStable <: Base.Sort.Algorithm end

struct StaticLengthSortOblivious{
  sorting_network_threshold, Opt<:Union{SmallDepth,SmallSize},
} <: Base.Sort.Algorithm end

const SLS = Union{StaticLengthSortStable,StaticLengthSortOblivious}

struct NegativeLength{length} <: Exception end

check(::StaticLengthSortStable) = nothing
check(::StaticLengthSortOblivious{m}) where {m} = (m < 0) && throw(NegativeLength{m}())

struct MergeStep{Opt<:Union{Stable,Tuple{Oblivious,Any}}} end
struct TrivialLeaf end
struct OSNLeaf{O} end

const Tree = BinaryTrees.Node{T,C} where {T, C<:NT{2}}

const MergeTree = Tree{MergeStep{Opt},C} where {Opt, C}
const TrivialTree = Tree{TrivialLeaf,NTuple{2,Nothing}}
const OSNTree = Tree{OSNLeaf{O},NTuple{2,Nothing}} where {O}

# instance of singleton type, could use the package...
merge_step_option(::MergeTree{Stable}) = Stable()
merge_step_option(::MergeTree{Tuple{T,S}}) where {T, S} = (T(), S())

sorted_impl(
  t::T, ::TrivialTree, ::Ord.Ordering,
) where {T<:Union{Tuple{},Tuple{Any}}} =
  t

sorted_impl(
  t::T, ::OSNTree{SD}, o::O,
) where {T<:TwoAndUp, SD, O<:Ord.Ordering} =
  OSN.sorted(Merging.new_minmax(o), t, SD())::rt(t)

function sorted_impl(
  t::T, mt::MT, o::O,
) where {T<:TwoAndUp, MT<:MergeTree, O<:Ord.Ordering}
  (l, r) = TupleUtils.split_in_half(t)
  subt = BinaryTrees.subtrees(mt)
  sl = sorted_impl(l, first(subt), o)
  sr = sorted_impl(r, last(subt), o)
  mso = merge_step_option(mt)
  Merging.merge(sl, sr, o, mso)::rt(t)
end

trivial_tree() = new_binary_tree(TrivialLeaf())::TrivialTree

osn_tree_impl(::Type{T}) where {T} = new_binary_tree(OSNLeaf{T}())::OSNTree{T}

osn_tree(::Type{SmallDepth}) = osn_tree_impl(OSN.Depth)
osn_tree(::Type{SmallSize} ) = osn_tree_impl(OSN.Size)

new_singleton_tree(::Val{0}, ::StaticLengthSortStable) = trivial_tree()
new_singleton_tree(::Val{1}, ::StaticLengthSortStable) = trivial_tree()
new_singleton_tree(::Val{0}, ::StaticLengthSortOblivious) = trivial_tree()
new_singleton_tree(::Val{1}, ::StaticLengthSortOblivious) = trivial_tree()

function new_singleton_tree_merge_step(::Type{T}, ::Val{m}, sls::SLS) where {T, m}
  mr = m >>> true
  ml = m - mr
  l = new_singleton_tree(Val(ml), sls)
  r = new_singleton_tree(Val(mr), sls)
  new_binary_tree(MergeStep{T}(), (l, r))::MergeTree{T}
end

new_singleton_tree_oblivious(
  ::Val{false}, ::Val{m}, sls::StaticLengthSortOblivious{n,O},
) where {m, n, O} =
  new_singleton_tree_merge_step(Tuple{Oblivious,O}, Val(m), sls)

new_singleton_tree_oblivious(
  ::Val{true}, ::Val, ::StaticLengthSortOblivious{n,SD},
) where {n, SD} =
  osn_tree(SD)

@Base.pure new_singleton_tree(::Val{m}, sls::StaticLengthSortOblivious{n}) where {m, n} =
  new_singleton_tree_oblivious(
    Val(length_is_supported_by_osn(Val(m), Val(n))), Val(m), sls,
  )

@Base.pure new_singleton_tree(::Val{m}, sls::StaticLengthSortStable) where {m} =
  new_singleton_tree_merge_step(Stable, Val(m), sls)

function sorted(
  t::T, sls::SLS, o::O,
) where {n, T<:NT{n}, O<:Ord.Ordering}
  tree = new_singleton_tree(Val(n), sls)
  sorted_impl(t, tree, o)::rt(t)
end

sorted(t::T, o::O) where {T<:Tuple, O<:Ord.Ordering} =
  sorted(t, StaticLengthSortStable(), o)

sorted(t::T, sls::SLS) where {T<:Tuple} = sorted(t, sls, Ord.Forward)
sorted(t::T) where {T<:Tuple} = sorted(t, Ord.Forward)

end
