# TupleSorting

[![PkgEval](https://JuliaCI.github.io/NanosoldierReports/pkgeval_badges/T/TupleSorting.svg)](https://JuliaCI.github.io/NanosoldierReports/pkgeval_badges/T/TupleSorting.html)
[![Aqua](https://raw.githubusercontent.com/JuliaTesting/Aqua.jl/master/badge.svg)](https://github.com/JuliaTesting/Aqua.jl)

Sort tuples efficiently and with good type inference. A recursive merge sort
approach.

Provides both stable and unstable sorting algorithms. A sort is stable when
it preserves the order among elements that compare equal. The default
algorithm is stable, but the unstable (oblivious comparison sort, sorting
network) algorithms may be much faster, depending on the types of the
elements of the tuple.

## Usage example

```julia-repl
julia> using TupleSorting

julia> const TS = TupleSorting
TupleSorting

julia> t = ntuple((_ -> rand(0:9)), Val(25))
(3, 2, 1, 0, 5, 0, 3, 0, 3, 7, 2, 8, 1, 3, 5, 7, 6, 6, 0, 9, 1, 8, 3, 1, 9)

julia> sorted(t)
(0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 3, 3, 3, 3, 3, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9)

julia> sorted(t, StaticLengthSortOblivious{20,TS.SmallSize}())
(0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 3, 3, 3, 3, 3, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9)

julia> sorted(t, Base.Order.Reverse)  # reverse order
(9, 9, 8, 8, 7, 7, 6, 6, 5, 5, 3, 3, 3, 3, 3, 2, 2, 1, 1, 1, 1, 0, 0, 0, 0)

julia> sorted(t, StaticLengthSortStable(), Base.Order.Forward)  # default arguments
(0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 3, 3, 3, 3, 3, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9)
```

Use the functionality existing in `Base.Order` for constructing complex orders,
e.g., `Base.Order.By`, `Base.Order.Lt`.

## Some benchmarks

Benchmarking code:

```julia
import Random, InteractiveUtils, BenchmarkTools, TupleSorting

const BT = BenchmarkTools
const TS = TupleSorting

function bench(io, name, b)
  print(io, name, ":  ")
  s = let io = io
    x -> show(io, "text/plain", x)
  end
  (s ∘ BT.run)(b)
  print(io, "\n\n")
  flush(io)
end

bench(name, b) = bench(stdout, name, b)

function map_to!(f::F, v::AbstractVector) where {F}
  len = length(v)
  (len < 150) && error("too small")
  for i ∈ 0:(len - 1)
    j = firstindex(v) + i
    v[j] = f(v[j])
  end
  v
end

sort!(v::AbstractVector, sls::S) where {S} =
  map_to!((t -> TS.sorted(t, sls, Base.Order.Forward)), v)

function new_tuple(::Type{T}, prg::P, ::Val{n}) where {T, P, n}
  f = let p = prg
    _ -> Random.rand(p, T)::T
  end
  ntuple(f, Val(n))::NTuple{n,T}
end

new_vector(
  ::Type{T}, ::Val{n}, length::Int = 500, prg::P = Random.Xoshiro(123456),
) where {T, n, P} =
  NTuple{n,T}[new_tuple(T, prg, Val(n)) for _ ∈ 1:length]

const TSSS = TS.SmallSize
const TSSD = TS.SmallDepth

const sls_stable = TS.StaticLengthSortStable()
const sls_ss = TS.StaticLengthSortOblivious{0,TSSS}()
const sls_sd = TS.StaticLengthSortOblivious{0,TSSD}()
const sls_ss_acc = TS.StaticLengthSortOblivious{50,TSSS}()
const sls_sd_acc = TS.StaticLengthSortOblivious{50,TSSD}()

const slss = (sls_stable, sls_ss, sls_sd, sls_ss_acc, sls_sd_acc)

const sls_names = (
  "sls_stable",
  "sls_ss    ",
  "sls_sd    ",
  "sls_ss_acc",
  "sls_sd_acc",
)

const slss_named = zip(sls_names, slss)

const vals = (map(Val, 46:2:50)...,)

# get everything compiled
for T ∈ (Int32, Float32), val ∈ vals, sls ∈ slss
  v = new_vector(T, val)
  sort!(v, sls)
end

BenchmarkTools.DEFAULT_PARAMETERS.evals   = 1
BenchmarkTools.DEFAULT_PARAMETERS.samples = 3000
BenchmarkTools.DEFAULT_PARAMETERS.seconds = 1000

for T ∈ (Int32, Float32), val ∈ vals, (sls_name, sls) ∈ slss_named
  bench((T, val, sls_name), BT.@benchmarkable sort!(v, $sls) setup=(v=new_vector($T,$val);))
end

println()
InteractiveUtils.versioninfo()
```

Results:

```
(Int32, Val{46}(), "sls_stable"):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  386.390 μs … 536.673 μs  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     391.689 μs               ┊ GC (median):    0.00%
 Time  (mean ± σ):   396.261 μs ±  15.144 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

  ▆█▇▆▆▆▅▄▄▂▂▂▁▁▁▁                                              ▁
  ██████████████████▇▆▇▇▆▇▆▅▆▆▆▅▄▄▄▅▆▄▃▅▆▄▄▅▃▄▅▅▅▄▄▃▅▅▅▆▅▄▄▅▆▄▄ █
  386 μs        Histogram: log(frequency) by time        469 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Int32, Val{46}(), "sls_ss    "):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  89.940 μs … 116.691 μs  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     90.101 μs               ┊ GC (median):    0.00%
 Time  (mean ± σ):   90.962 μs ±   3.109 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

  █▃           ▁                                                
  ███▇▆▅▇▆▅▅▅▅██▄▃▄▄▄▄▄▅▄▅▅▆▆▅▅▆▅▄▅▄▄▄▄▃▃▄▁▁▄▁▅▃▄▄▄▅▅▄▅▄▅▅▅▅▄▄ █
  89.9 μs       Histogram: log(frequency) by time       107 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Int32, Val{46}(), "sls_sd    "):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  98.315 μs … 158.078 μs  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     98.666 μs               ┊ GC (median):    0.00%
 Time  (mean ± σ):   99.712 μs ±   3.885 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

  ██        ▁▂                                                  
  ███▆▅▇▇▅▅▆██▃▅▃▃▆▄▆▅▅▄▄▄▂▅▃▂▅▃▄▅▄▄▃▄▄▄▄▂▃▃▄▃▄▄▄▅▃▄▄▃▄▄▄▃▄▂▄▃ █
  98.3 μs       Histogram: log(frequency) by time       119 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Int32, Val{46}(), "sls_ss_acc"):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  86.353 μs … 127.761 μs  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     86.503 μs               ┊ GC (median):    0.00%
 Time  (mean ± σ):   87.889 μs ±   4.671 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

  █▃       ▁                                                    
  ██▇▆▇▆▅▆██▆▄▄▅▄▄▄▅▅▃▅▄▅▄▄▄▅▅▅▄▁▄▄▆▅▆▃▅▅▆▆▆▅▅▆▅▅▄▄▅▅▅▄▅▄▃▄▄▄▅ █
  86.4 μs       Histogram: log(frequency) by time       110 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Int32, Val{46}(), "sls_sd_acc"):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  93.496 μs … 141.508 μs  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     93.627 μs               ┊ GC (median):    0.00%
 Time  (mean ± σ):   94.720 μs ±   4.067 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

  █▃        ▂                                                   
  ██▇▆▆▇▅▆▄██▇▁▅▄▆▄▅▅▆▆▆▅▄▆▃▄▅▅▄▃▄▅▄▄▃▄▅▁▄▁▄▃▄▄▄▄▃▄▅▄▅▆▄▅▅▁▅▄▅ █
  93.5 μs       Histogram: log(frequency) by time       115 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Int32, Val{48}(), "sls_stable"):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  428.990 μs … 539.409 μs  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     434.214 μs               ┊ GC (median):    0.00%
 Time  (mean ± σ):   437.235 μs ±   9.975 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

    ▇█▆▄▃                                                        
  ▃██████████▅▄▄▄▃▃▃▃▃▃▃▂▂▃▃▃▃▂▂▃▂▂▂▂▂▂▂▂▂▂▂▂▂▂▂▂▂▂▂▂▂▂▁▂▂▁▂▁▂▂ ▃
  429 μs           Histogram: frequency by time          480 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Int32, Val{48}(), "sls_ss    "):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  92.976 μs … 149.332 μs  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     93.737 μs               ┊ GC (median):    0.00%
 Time  (mean ± σ):   95.167 μs ±   4.903 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

  ▇█▇▅▁     ▁                                                  ▁
  █████▇▇▆▆███▆▄▄▃▅▄▄▃▃▃▅▅▅▄▃▃▅▆▆▅▅▆▃▃▅▄▅▄▅▅▆▅▆▆▆▅▅▆▅▆▅▄▅▅▅▃▃▅ █
  93 μs         Histogram: log(frequency) by time       117 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Int32, Val{48}(), "sls_sd    "):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  103.325 μs … 157.187 μs  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     104.117 μs               ┊ GC (median):    0.00%
 Time  (mean ± σ):   105.225 μs ±   4.228 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

   █▇▅▂      ▂▁                                                 ▁
  ▇████▇▇▆▅▆▇██▆▁▅▄▅▆▆▅▆▆▅▅▅▅▅▅▆▅▃▃▅▁▅▅▄▅▃▁▅▆▆▆▄▅▅▅▅▅▅▄▅▅▄▄▃▄▃▅ █
  103 μs        Histogram: log(frequency) by time        126 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Int32, Val{48}(), "sls_ss_acc"):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  92.644 μs … 162.887 μs  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     93.487 μs               ┊ GC (median):    0.00%
 Time  (mean ± σ):   95.065 μs ±   5.434 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

   ██▂      ▂                                                  ▁
  ████▇▇█▆▇██▅▅▃▆▅▆▆▆▆▇▅▆▄▄▆▆▆▅▃▅▅▆▅▅▅▅▃▆▅▇▆▆▆▇▆▇▆▅▅▃▆▇▆▅▁▆▅▄▆ █
  92.6 μs       Histogram: log(frequency) by time       118 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Int32, Val{48}(), "sls_sd_acc"):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):   99.819 μs … 153.570 μs  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     100.650 μs               ┊ GC (median):    0.00%
 Time  (mean ± σ):   101.655 μs ±   3.988 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

  ▂▆█▇▁       ▁▂                                                ▁
  █████▇▆▆▆▅▆▆███▆▁▄▅▅▅▅▆▆▆▆▃▅▅▅▅▅▅▅▅▅▅▅▄▆▅▃▅▁▃▃▅▆▅▅▃▄▃▁▅▅▄▆▃▅▅ █
  99.8 μs       Histogram: log(frequency) by time        119 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Int32, Val{50}(), "sls_stable"):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  475.447 μs … 667.450 μs  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     481.744 μs               ┊ GC (median):    0.00%
 Time  (mean ± σ):   488.003 μs ±  20.071 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

  ▆███▇▆▅▄▃▂▁▁▁▁                                                ▁
  ███████████████▇▇██▇▆▇▇▅▅▆▇▆▇▅▆▄▅▄▆▃▆▅▄▄▅▆▄▅▆▄▃▅▄▄▄▃▇▅▅▅▄▆▅▄▆ █
  475 μs        Histogram: log(frequency) by time        581 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Int32, Val{50}(), "sls_ss    "):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  101.301 μs … 178.777 μs  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     101.502 μs               ┊ GC (median):    0.00%
 Time  (mean ± σ):   103.180 μs ±   5.802 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

  █▂      ▁                                                      
  ██▆▆▆▆▅▇█▆▄▃▄▄▅▅▆▅▄▅▅▅▅▂▄▄▄▄▄▄▅▄▂▅▄▅▄▅▄▅▆▄▅▅▄▅▄▃▄▄▄▄▄▂▃▃▃▂▂▄▄ █
  101 μs        Histogram: log(frequency) by time        129 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Int32, Val{50}(), "sls_sd    "):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  111.010 μs … 158.119 μs  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     111.551 μs               ┊ GC (median):    0.00%
 Time  (mean ± σ):   112.699 μs ±   3.992 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

  ▄█▂        ▁▂                                                  
  ███▇▇▆▇▆▅▆▇██▆▅▃▅▅▅▄▅▅▆▅▆▅▆▆▅▆▄▄▅▅▆▅▅▁▃▅▇▅▄▅▆▄▅▆▅▅▅▅▅▃▆▅▁▃▄▅▅ █
  111 μs        Histogram: log(frequency) by time        132 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Int32, Val{50}(), "sls_ss_acc"):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  101.281 μs … 156.796 μs  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     101.452 μs               ┊ GC (median):    0.00%
 Time  (mean ± σ):   102.549 μs ±   4.186 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

  █▄         ▂                                                   
  ██▇▆▆█▇▅▅▅██▄▄▄▅▅▄▃▅▆▅▅▁▄▅▃▄▅▅▄▅▄▅▅▃▄▅▃▄▅▄▅▅▅▅▅▅▅▄▅▃▃▃▄▄▃▃▃▄▅ █
  101 μs        Histogram: log(frequency) by time        122 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Int32, Val{50}(), "sls_sd_acc"):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  108.034 μs … 154.301 μs  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     108.344 μs               ┊ GC (median):    0.00%
 Time  (mean ± σ):   109.579 μs ±   4.334 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

  █▅        ▂                                                    
  ██▇▆▆▆▆▆▅██▆▅▅▄▆▆▇▅▅▇▆▆▆▅▄▆▃▃▅▅▆▃▃▄▅▅▁▁▅▃▆▅▅▅▅▆▅▆▃▄▅▄▃▅▅▄▃▄▄▅ █
  108 μs        Histogram: log(frequency) by time        131 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Float32, Val{46}(), "sls_stable"):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  763.351 μs … 917.732 μs  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     771.893 μs               ┊ GC (median):    0.00%
 Time  (mean ± σ):   776.372 μs ±  15.844 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

   ▇█▇██▇▇▇▅▅▄▄▂▂▁▁▁▁                                           ▂
  ███████████████████████▆▇▆▇▅▆▇▆▆▆▄▆▃▃▇▃▆▆▆▆▄▅▆▃▆▆▃▄▃▅▅▄▅▅▅▅▃▆ █
  763 μs        Histogram: log(frequency) by time        852 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Float32, Val{46}(), "sls_ss    "):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  786.685 μs … 896.753 μs  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     799.229 μs               ┊ GC (median):    0.00%
 Time  (mean ± σ):   802.598 μs ±  11.089 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

         ▁▃█▅▄▂▂                                                 
  ▁▁▁▂▂▄▇████████▅▅▅▄▃▃▄▄▂▃▂▃▂▂▂▂▂▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁ ▂
  787 μs           Histogram: frequency by time          850 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Float32, Val{46}(), "sls_sd    "):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  825.939 μs … 995.079 μs  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     834.495 μs               ┊ GC (median):    0.00%
 Time  (mean ± σ):   837.514 μs ±  11.412 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

   ▁▅▃▄█▇▂▃▃▁      ▁                                             
  ▄██████████▆██▇▆▇█▆▅▅▅▃▃▄▃▃▃▃▂▃▂▂▂▂▂▂▂▃▂▂▂▂▂▂▂▂▂▂▂▂▂▂▂▂▁▂▂▂▂▂ ▄
  826 μs           Histogram: frequency by time          885 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Float32, Val{46}(), "sls_ss_acc"):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  804.409 μs … 914.807 μs  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     814.653 μs               ┊ GC (median):    0.00%
 Time  (mean ± σ):   817.421 μs ±  10.840 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

    ▄▅▄▁▄▇█▆▁ ▂▃                                                 
  ▃▅█████████████▇▇██▇▆▇▆▆▆▆▆▅▅▄▄▃▃▃▂▃▂▂▂▂▂▂▂▂▂▁▃▂▂▂▂▂▂▂▂▂▂▂▂▂▂ ▄
  804 μs           Histogram: frequency by time          858 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Float32, Val{46}(), "sls_sd_acc"):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  824.326 μs … 955.494 μs  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     832.612 μs               ┊ GC (median):    0.00%
 Time  (mean ± σ):   835.325 μs ±  10.005 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

     ▅▆▃▄█▅▄▁▁▁▁                                                 
  ▃▄████████████▇▇▆▆▅▆▅▅▄▄▃▄▃▃▃▃▃▃▃▂▂▂▃▂▂▂▂▂▂▂▂▂▂▂▂▂▂▁▂▂▂▁▁▂▁▂▂ ▄
  824 μs           Histogram: frequency by time          877 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Float32, Val{48}(), "sls_stable"):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  800.261 μs … 956.285 μs  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     807.449 μs               ┊ GC (median):    0.00%
 Time  (mean ± σ):   811.671 μs ±  15.125 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

  ▄▇████▇▇▆▅▄▃▃▃▃▃▂▂▁▁▁                                         ▂
  █████████████████████▇▇▇▇▇▆▇▇▇▆▇▇█▇▇▆▅▅▆▆▆▆▆▅▇▅▆▆▁▄▁▅▅▅▄▄▆▅▅▆ █
  800 μs        Histogram: log(frequency) by time        881 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Float32, Val{48}(), "sls_ss    "):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  833.603 μs … 921.700 μs  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     844.895 μs               ┊ GC (median):    0.00%
 Time  (mean ± σ):   847.144 μs ±   9.622 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

     ▃▆▇▄▄▇█▆▄▄▄▂▂▂▂▁  ▁▂▁                                       
  ▁▂▄█████████████████████▆▇▆▅▅▅▄▄▃▄▂▃▃▃▂▂▂▂▂▂▁▂▁▂▁▂▁▂▁▂▁▁▁▁▁▁▁ ▄
  834 μs           Histogram: frequency by time          881 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Float32, Val{48}(), "sls_sd    "):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  871.124 μs …  1.264 ms  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     879.691 μs              ┊ GC (median):    0.00%
 Time  (mean ± σ):   888.172 μs ± 16.410 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

   ▁█▆▃▃▄▂                                                      
  ▂████████▆▄▄▂▃▃▂▂▂▂▁▁▂▂▁▁▂▁▂▁▁▂▄██▅▇█▇▅▅▃▄▃▂▂▂▂▂▂▂▂▂▂▁▁▁▁▁▁▁ ▃
  871 μs          Histogram: frequency by time          925 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Float32, Val{48}(), "sls_ss_acc"):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  832.552 μs … 932.771 μs  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     844.354 μs               ┊ GC (median):    0.00%
 Time  (mean ± σ):   846.370 μs ±  10.125 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

     ▃▇█▄▄▄▂▁▁                                                   
  ▁▂▅█████████▇▆▇▇▇█▇▇▇▇▇███▇▆▅▄▄▃▃▃▃▃▂▂▃▂▂▂▂▂▂▂▁▁▁▁▁▁▁▁▁▁▁▁▂▁▁ ▃
  833 μs           Histogram: frequency by time          881 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Float32, Val{48}(), "sls_sd_acc"):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  866.656 μs … 959.221 μs  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     873.584 μs               ┊ GC (median):    0.00%
 Time  (mean ± σ):   875.541 μs ±   7.631 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

    ▃███▇▅▅▄▅▅▂▂▂  ▁▁                                            
  ▃▅██████████████▇██▇▆▆▆▅▅▄▄▄▄▄▃▃▃▄▃▃▃▃▂▂▂▂▃▂▂▂▂▂▂▂▂▂▂▂▂▂▂▂▁▂▂ ▄
  867 μs           Histogram: frequency by time          905 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Float32, Val{50}(), "sls_stable"):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  838.573 μs …  1.051 ms  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     846.092 μs              ┊ GC (median):    0.00%
 Time  (mean ± σ):   850.552 μs ± 15.923 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

   ▆█▇▇▇▇▆▅▅▄▃▃▂▁▂▁ ▁                                          ▂
  ███████████████████████▇▇▇█▇█▇█▇▇▆▅▇▆▆▆▇▇▄▆▇▆▅▅▅▄▆▄▄▄▄▄▄▆▁▄▅ █
  839 μs        Histogram: log(frequency) by time       921 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Float32, Val{50}(), "sls_ss    "):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  897.054 μs …  1.138 ms  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     913.374 μs              ┊ GC (median):    0.00%
 Time  (mean ± σ):   918.749 μs ± 21.188 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

      ▂▃▇█▅▄▂▄ ▁                                                
  ▂▄▅▄██████████▇▇▇▆▅▆▅▄▄▃▄▃▃▃▃▃▂▃▂▃▂▂▂▂▂▃▂▂▂▂▂▂▁▂▂▂▂▂▂▂▂▂▂▂▂▂ ▄
  897 μs          Histogram: frequency by time          991 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Float32, Val{50}(), "sls_sd    "):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  925.157 μs …  1.050 ms  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     932.360 μs              ┊ GC (median):    0.00%
 Time  (mean ± σ):   935.076 μs ± 10.384 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

    ▃▅▇▇█▆▄▄▂                                                   
  ▂▆██████████▆▇▅▅▄▄▃▃▂▃▂▂▂▂▂▂▂▁▁▂▁▂▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁ ▃
  925 μs          Histogram: frequency by time          979 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Float32, Val{50}(), "sls_ss_acc"):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  905.659 μs …  1.013 ms  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     915.578 μs              ┊ GC (median):    0.00%
 Time  (mean ± σ):   918.205 μs ± 11.151 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

   ▁▆▄ ▅█▅▄█▇▄▃▄                                                
  ▂███████████████▇▆▅▄▄▅▃▄▃▃▃▂▃▂▂▂▁▂▁▁▁▂▁▁▁▁▁▁▁▁▁▁▁▂▁▁▁▁▁▁▁▁▁▁ ▃
  906 μs          Histogram: frequency by time          964 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.

(Float32, Val{50}(), "sls_sd_acc"):  BenchmarkTools.Trial: 3000 samples with 1 evaluation.
 Range (min … max):  907.783 μs …  1.091 ms  ┊ GC (min … max): 0.00% … 0.00%
 Time  (median):     916.304 μs              ┊ GC (median):    0.00%
 Time  (mean ± σ):   921.031 μs ± 14.384 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

    ▇██▆▅▃         ▁                                            
  ▄████████▇▆▄▄▄▅▇███▆▇▅▄▄▄▄▃▃▃▃▃▃▃▃▂▂▂▂▂▂▂▂▂▂▂▂▂▂▂▂▂▂▂▁▁▂▂▁▁▂ ▄
  908 μs          Histogram: frequency by time          972 μs <

 Memory estimate: 0 bytes, allocs estimate: 0.


Julia Version 1.9.4
Commit 8e5136fa297 (2023-11-14 08:46 UTC)
Build Info:
  Official https://julialang.org/ release
Platform Info:
  OS: Linux (x86_64-linux-gnu)
  CPU: 8 × AMD Ryzen 3 5300U with Radeon Graphics
  WORD_SIZE: 64
  LIBM: libopenlibm
  LLVM: libLLVM-14.0.6 (ORCJIT, znver2)
  Threads: 8 on 8 virtual cores
Environment:
  JULIA_NUM_PRECOMPILE_TASKS = 3
  JULIA_PKG_PRECOMPILE_AUTO = 0
  JULIA_EDITOR = code
  JULIA_NUM_THREADS = 8
```

Some things I notice:

1. Sorting networks provide a significant speedup for sorting `Int32`.

2. Small-size sorting networks seem to be faster than small-depth ones.
   The reverse would hold on a GPU, I guess.
