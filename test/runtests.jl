import TupleSorting, Aqua
using Test: @testset, @test, @inferred

const TS = TupleSorting

rand_missing() = rand() < 0.1 ? missing : rand()

example_tuples(::V) where {V<:Val} = (
  ntuple((_ -> rand(Bool)),          V()),
  ntuple((_ -> rand(Base.OneTo(3))), V()),
  ntuple((_ -> rand(Base.OneTo(4))), V()),
  ntuple((_ -> rand(Base.OneTo(5))), V()),
  ntuple((_ -> rand(Base.OneTo(6))), V()),
  ntuple((_ -> rand()),              V()),
  ntuple((_ -> rand_missing()),      V()),
)

example_tuples(n::Int) = example_tuples(Val(n))

const example_sls = let
  ns = (0, 1, 2, 3, 4, 8, 20)
  opts = (TS.SmallDepth, TS.SmallSize)
  it = Iterators.product(ns, opts)
  f = x -> TS.StaticLengthSortOblivious{x...}()
  (map(f, it)..., TS.StaticLengthSortStable())
end

const order_shared = Base.Order.Forward

const order_stable = Base.Order.By(first, order_shared)

function test_sorting_shared(
  t::T, sls, o::O,
) where {T<:Tuple, O<:Base.Order.Ordering}
  @test issorted(@inferred TS.sorted(t, sls, o))
  @test iszero(@allocated TS.sorted(t, sls, o))
end

test_sorting(t::T, sls) where {T<:Tuple} =
  test_sorting_shared(t, sls, order_shared)

function test_sorting(
  t::NTuple{n,Any}, sls::TS.StaticLengthSortStable,
) where {n}
  test_sorting_shared(t, sls, order_shared)

  f = let t = t
    i -> (t[i], i)
  end

  u = ntuple(f, Val(n))

  test_sorting_shared(u, sls, order_stable)
end

test_sorting(t::T) where {T<:Tuple} =
  for sls ∈ example_sls
    test_sorting(t, sls)
  end

@testset "TupleSorting.jl" begin
  @testset "Code quality (Aqua.jl)" begin
    Aqua.test_all(TS)
  end

  @testset "sorting $n" for n ∈ 0:40, t ∈ example_tuples(n)
    test_sorting(t)
  end
end
